﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace QuickPay.Common.Ententions
{
    public static class ClaimsExtention
    {
        public static ClaimsPrincipal Claims(this string token)
        {
            try
            {
                var key = Encoding.ASCII.GetBytes("asdv234234^&%&^%&^hjsdfb2%%%");
                var handler = new JwtSecurityTokenHandler();
                var tokenSecure = handler.ReadToken(token) as SecurityToken;

                var validations = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };

                var claims = handler.ValidateToken(token, validations, out tokenSecure);
                return claims;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
