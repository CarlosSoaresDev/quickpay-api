﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace QuickPay.Common.Ententions
{
    public static class StringExtention
    {
        public static string Encode(this string value)
        {
            string criptKey;
            Byte[] cript = System.Text.ASCIIEncoding.ASCII.GetBytes(value);
            criptKey = Convert.ToBase64String(cript);
            return criptKey;
        }

        public static string Decode(this string value)
        {
            string criptKey;
            Byte[] cript = Convert.FromBase64String(value);
            criptKey = System.Text.ASCIIEncoding.ASCII.GetString(cript);
            return criptKey;
        }

        public static string ToHashMd5(this string objToEncript)
        {
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(objToEncript));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        public static ClaimsPrincipal Claims(this string token)
        {
            try
            {
                var formatToken = token.Substring(7);

                string secret = SercreKey();
                var key = Encoding.ASCII.GetBytes(secret);
                var handler = new JwtSecurityTokenHandler();
                var tokenSecure = handler.ReadToken(formatToken) as SecurityToken;

                var validations = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };

                var claims = handler.ValidateToken(formatToken, validations, out tokenSecure);
                return claims;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public static string SercreKey()
        {
            return "MCSmasterKey@superKeY#&2018$$!0!";
        }
    }
}
