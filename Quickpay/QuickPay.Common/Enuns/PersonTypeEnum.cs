﻿namespace QuickPay.Common.Enuns
{
    public enum PersonTypeEnum
    {
        Admin = 1,
        User = 2,
    }
}
