﻿namespace QuickPay.Common.Enuns
{
    public enum TokenStatusEnum
    {
        Accept = 1,
        NotAccepted = 2,
        InactiveUser = 3
    }
}
