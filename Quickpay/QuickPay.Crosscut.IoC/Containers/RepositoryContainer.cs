﻿

using Microsoft.Extensions.DependencyInjection;
using QuickPay.Data.Repository.Contracts.BankModule;
using QuickPay.Data.Repository.Contracts.PersonModule;
using QuickPay.Data.Repository.Services.BankModule;
using QuickPay.Data.Repository.Services.PersonModule;

namespace QuickPay.Crosscut.IoC.Containers
{
    public static class RepositoryContainer
    {
        public static IServiceCollection InitializerRepositoryContainer(this IServiceCollection services)
        {
            services.AddTransient<IBankRepository, BankRepository>();
            services.AddTransient<IPersonRepository, PersonRepository>();

            return services;
        }
    }
}
