﻿using Microsoft.Extensions.DependencyInjection;
using QuickPay.Service.Contracts.AuthModule;
using QuickPay.Service.Contracts.BankModule;
using QuickPay.Service.Contracts.PersonModule;
using QuickPay.Service.DTO.AuthModule;
using QuickPay.Service.Services.BankModule;
using QuickPay.Service.Services.PersonModule;

namespace QuickPay.Crosscut.IoC.Containers
{
    public static class ServiceContainer
    {
        public static IServiceCollection InitializeServiceContainer(this IServiceCollection services)
        {
            services.AddTransient<IBankService, BankService>();
            services.AddTransient<IPersonService, PersonService>();
            services.AddTransient<IAuthService, AuthService>();

            return services;
        }
    }
}
