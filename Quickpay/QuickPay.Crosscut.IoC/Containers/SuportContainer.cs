﻿using Microsoft.Extensions.DependencyInjection;
using QuickPay.Service.Adapter;

namespace QuickPay.Crosscut.IoC.Containers
{
    public static class SuportContainer
    {
        public static IServiceCollection InitializeSuportContainer(this IServiceCollection services)
        {
            #region AutoMapper
            services.AddTransient<IAutoMapperObjectAdapter, AutoMapperObjectAdapter>();
            #endregion

            return services;
        }
    }
}
