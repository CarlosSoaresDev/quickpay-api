﻿using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using QuickPay.Common.Ententions;
using QuickPay.Domain.Entities;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace QuickPay.Crosscut.Security.JwtProvider
{
    public class QuickPayJwtProvider
    {
        private readonly RequestDelegate _next;

        public QuickPayJwtProvider(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            if (!token.IsNull())
                AttachUserToContext(context, token);

            await _next(context);
        }

        private void AttachUserToContext(HttpContext context, string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes("asdv234234^&%&^%&^hjsdfb2%%%");
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,

                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                var personUser = new Person();

                personUser.PersonID = long.Parse(jwtToken.Claims.First(x => x.Type == "personID").Value);

                context.Items["PersonUser"] = personUser;
            }
            catch
            {

            }
        }

        public static string GenerateJwtToken(Person user)
        {
            var claims = new List<Claim>(){
                new Claim(Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames.Sub, user.Name),
                new Claim(Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames.Email, user.Email),
                new Claim("name", user.Name),
                new Claim("date", user.CreatedDateTime.ToString("dd/MM/yyyy")),
                new Claim("isActive", user.IsActive.ToString()),
                new Claim("personID", user.PersonID.ToString()),
                new Claim(ClaimTypes.Role, "Acess"),
                user.Type.HasFlag(Common.Enuns.PersonTypeEnum.Admin) ?
                new Claim(ClaimTypes.Role, "Admin") :
                new Claim(ClaimTypes.Role, "User")
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("asdv234234^&%&^%&^hjsdfb2%%%");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}