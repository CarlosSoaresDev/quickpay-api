﻿using Microsoft.EntityFrameworkCore;
using QuickPay.Domain.Entities;

namespace QuickPay.Data.Context
{
    public class QuickPayEntities : DbContext
    {
        public QuickPayEntities(DbContextOptions<QuickPayEntities> options)
            : base(options)
        { }

        public DbSet<Bank> Banks { get; set; }
        public DbSet<Person> People { get; set; }

    }
}
