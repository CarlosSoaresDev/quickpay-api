﻿using QuickPay.Domain.Entities;
using System;
using System.Linq;

namespace QuickPay.Data.Repository.Contracts.BankModule
{
    public interface IBankRepository
    {
        IQueryable<Bank> GetAll();

        IQueryable<Bank> Get(Func<Bank, bool> predicate);

        Bank Find(params object[] key);

        void Update(Bank entity);

        void Add(Bank entity);

        void Delete(Bank entity);
    }
}
