﻿using QuickPay.Domain.Entities;
using System;
using System.Linq;

namespace QuickPay.Data.Repository.Contracts.PersonModule
{
    public interface IPersonRepository
    {
        IQueryable<Person> GetAll();

        IQueryable<Person> Get(Func<Person, bool> predicate);

        Person Find(params object[] key);

        void Update(Person entity);

        void Add(Person entity);

        void Delete(Person entity);
    }
}
