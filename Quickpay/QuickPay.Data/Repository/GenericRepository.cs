﻿using Microsoft.EntityFrameworkCore;
using QuickPay.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickPay.Data.Repository
{
    public class GenericRepository<TEntity> : IDisposable, IGenericRepository<TEntity> where TEntity : class
    {
        #region Fields       
        private readonly QuickPayEntities _quickPayEntities;
        #endregion

        #region Constructor
        public GenericRepository(QuickPayEntities quickPayEntities) => _quickPayEntities = quickPayEntities;
        #endregion

        #region Methods sync
        public IQueryable<TEntity> GetAll()
        {
            return _quickPayEntities.Set<TEntity>().AsNoTracking();
        }

        public IQueryable<TEntity> Get(Func<TEntity, bool> predicate)
        {
            return GetAll().Where(predicate).AsQueryable();
        }

        public TEntity Find(params object[] key)
        {
            return _quickPayEntities.Set<TEntity>().Find(key);
        }

        public void Update(TEntity entity)
        {
            _quickPayEntities.Entry(entity).State = EntityState.Modified;
            _quickPayEntities.SaveChanges();
        }

        public void Add(TEntity entity)
        {
            _quickPayEntities.Set<TEntity>().Add(entity);
            _quickPayEntities.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            _quickPayEntities.Set<TEntity>().Remove(entity);
            _quickPayEntities.SaveChanges();
        }

        public void Dispose()
        {
            _quickPayEntities.Dispose();
        }
        #endregion

        #region Methods Async
        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _quickPayEntities.Set<TEntity>().ToListAsync();
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            _quickPayEntities.Add(entity);
            await _quickPayEntities.SaveChangesAsync();
            return entity;
        }

        public async Task<TEntity> FindAsync(params object[] key)
        {
            return await _quickPayEntities.Set<TEntity>().FindAsync(key);
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            _quickPayEntities.Update(entity);
            await _quickPayEntities.SaveChangesAsync();
            return entity;
        }

        public async Task<int> DeleteAsync(TEntity entity)
        {
            int rowsAffected = 0;
            _quickPayEntities.Remove(entity);
            rowsAffected = await _quickPayEntities.SaveChangesAsync();

            return rowsAffected;
        }
        #endregion
    }
}
