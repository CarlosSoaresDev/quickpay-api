﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace QuickPay.Data.Repository
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        #region Sync   
        IQueryable<TEntity> GetAll();

        IQueryable<TEntity> Get(Func<TEntity, bool> predicate);

        TEntity Find(params object[] key);

        void Update(TEntity entity);

        void Add(TEntity entity);

        void Delete(TEntity entity);
        #endregion

        #region Async   
        Task<TEntity> AddAsync(TEntity entity);

        Task<TEntity> UpdateAsync(TEntity entity);

        Task<int> DeleteAsync(TEntity entity);
        #endregion
    }
}