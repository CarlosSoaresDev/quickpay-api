﻿using QuickPay.Data.Context;
using QuickPay.Data.Repository.Contracts.BankModule;
using QuickPay.Domain.Entities;

namespace QuickPay.Data.Repository.Services.BankModule
{
    public class BankRepository : GenericRepository<Bank>,IBankRepository
    {
        #region Constructor
        public BankRepository(QuickPayEntities quickPayEntities) :base(quickPayEntities)
        {
        }
        #endregion
    }
}
