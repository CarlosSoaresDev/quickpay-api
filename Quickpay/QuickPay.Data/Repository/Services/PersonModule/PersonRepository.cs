﻿using QuickPay.Data.Context;
using QuickPay.Data.Repository.Contracts.PersonModule;
using QuickPay.Domain.Entities;

namespace QuickPay.Data.Repository.Services.PersonModule
{

    public class PersonRepository : GenericRepository<Person>, IPersonRepository
    {
        #region Constructor
        public PersonRepository(QuickPayEntities quickPayEntities) : base(quickPayEntities)
        {
        }
        #endregion
    }
}
