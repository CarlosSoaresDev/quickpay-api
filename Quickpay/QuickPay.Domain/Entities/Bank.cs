﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuickPay.Domain.Entities
{
    [Table("Bank")]
    public class Bank
    {
        [Key]
        public long BankID { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public string About { get; set; }
        public string IconUrl { get; set; }
        public bool IsActive { get; set; } = true;
        public DateTime CreatedDateTime { get; set; } = DateTime.Now;
    }
}
