﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuickPay.Service.Contracts.AuthModule;
using QuickPay.Service.DTO.PersonModule;
using System;
using System.Collections.Generic;

namespace QuickPay.Presentation.WebApi.Controllers.AuthModule
{

    [ApiController]
    [Route("api/Auth")]
    public class AuthController : ControllerBase
    {
        #region Fields
        private readonly IAuthService _authService;
        #endregion

        #region Constructor
        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }
        #endregion

        #region Methods

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<long>))]
        [HttpPost("SignIn")]
        public IActionResult SingInUser([FromBody] PersonUserDTO personUser)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                return Ok(_authService.SignIn(personUser));
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is Exception)
            {
                throw new Exception(ex.Message);
            }
        }


        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<long>))]
        [HttpPost("SignUp")]
        public IActionResult SingUpUser([FromBody] PersonUserDTO personUser)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                return Ok(_authService.SignUp(personUser));
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is Exception)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion
    }
}

