﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuickPay.Common.Ententions;
using QuickPay.Presentation.WebApi.Helper;
using QuickPay.Service.Contracts.BankModule;
using QuickPay.Service.DTO.BankModule;
using System;
using System.Collections.Generic;

namespace QuickPay.Presentation.WebApi.Controllers.BankModule
{
    [ApiController]
    [Route("api/Bank")]
    public class BankController : HeaderManager
    {
        #region Fields
        private readonly IBankService _bankService;
        #endregion

        #region Constructor
        public BankController(IBankService bankService) => _bankService = bankService;
        #endregion

        #region Methods

        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<BankDTO>))]
        [HttpGet]
        public IActionResult GetAllBanks()
        {
            try
            {
                if (PersonID == 0)
                    return BadRequest();


                return Ok(_bankService.GetAll());
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is Exception)
            {
                throw new Exception(ex.Message);
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BankDTO))]
        [HttpGet("{bankID}")]
        public IActionResult GetCityByID([FromRoute] int bankID)
        {
            try
            {
                if (bankID <= 0)
                    return BadRequest();

                var bank = _bankService.GetByID(bankID);

                if (bank.IsNull())
                    return NotFound();

                return Ok(bank);
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is Exception)
            {
                throw new Exception(ex.Message);
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(long))]
        [HttpPut("{bankID}")]
        public IActionResult PutCity([FromRoute] int bankID, [FromBody] BankDTO bank)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (bankID <= 0)
                    return BadRequest();

                return Ok(_bankService.Update(bank));
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is Exception)
            {
                throw new Exception("Oops! There is a problem!", ex);
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<long>))]
        [HttpPost]
        public IActionResult PostCity([FromBody] BankDTO bank)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                return Ok(_bankService.Create(bank));
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is Exception)
            {
                throw new Exception(ex.Message);
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<BankDTO>))]
        [HttpPost("Filter")]
        public IActionResult GetCityByFilter(BankDTO bank)
        {
            try
            {
                return Ok(_bankService.GetAllByFilter(bank));
            }

            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is Exception)
            {
                throw new Exception(ex.Message);
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<long>))]
        [HttpDelete("{bankID}")]
        public IActionResult DeleteCity([FromRoute] int bankID)
        {
            try
            {
                if (bankID <= 0)
                    return BadRequest();

                return Ok(_bankService.Delete(bankID));
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is Exception)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}