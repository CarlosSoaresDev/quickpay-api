﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuickPay.Common.Ententions;
using QuickPay.Service.Contracts.PersonModule;
using QuickPay.Service.DTO.PersonModule;
using System;
using System.Collections.Generic;

namespace QuickPay.Presentation.WebApi.Controllers.PersonModule
{

    [ApiController]
    [Route("api/Person")]
    public class PersonController : ControllerBase
    {
        #region Fields
        private readonly IPersonService _personService;
        #endregion

        #region Constructor
        public PersonController(IPersonService personService) => _personService = personService;
        #endregion

        #region Methods

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<PersonDTO>))]
        [HttpGet]
        public IActionResult GetAllBanks()
        {
            try
            {
                return Ok(_personService.GetAll());
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is Exception)
            {
                throw new Exception(ex.Message);
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PersonDTO))]
        [HttpGet("{bankID}")]
        public IActionResult GetCityByID([FromRoute] int bankID)
        {
            try
            {
                if (bankID <= 0)
                    return BadRequest();

                var bank = _personService.GetByID(bankID);

                if (bank.IsNull())
                    return NotFound();

                return Ok(bank);
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is Exception)
            {
                throw new Exception(ex.Message);
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(long))]
        [HttpPut("{bankID}")]
        public IActionResult PutCity([FromRoute] int bankID, [FromBody] PersonDTO bank)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (bankID <= 0)
                    return BadRequest();

                return Ok(_personService.Update(bank));
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is Exception)
            {
                throw new Exception("Oops! There is a problem!", ex);
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<long>))]
        [HttpPost]
        public IActionResult PostCity([FromBody] PersonDTO bank)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                return Ok(_personService.Create(bank));
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is Exception)
            {
                throw new Exception(ex.Message);
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<PersonDTO>))]
        [HttpPost("Filter")]
        public IActionResult GetCityByFilter(PersonDTO bank)
        {
            try
            {
                return Ok(_personService.GetAllByFilter(bank));
            }

            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is Exception)
            {
                throw new Exception(ex.Message);
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<long>))]
        [HttpDelete("{bankID}")]
        public IActionResult DeleteCity([FromRoute] int bankID)
        {
            try
            {
                if (bankID <= 0)
                    return BadRequest();

                return Ok(_personService.Delete(bankID));
            }
            catch (AggregateException ex) when (ex.InnerException != null && ex.InnerException is Exception)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}