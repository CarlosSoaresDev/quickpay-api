﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuickPay.Common.Ententions;
using System;
using System.Linq;

namespace QuickPay.Presentation.WebApi.Helper
{
    public class HeaderManager : ControllerBase
    {
        private System.Security.Claims.ClaimsPrincipal HeaderAuthorization(HttpRequest request)
        {
            var context = request;
            var headers = context.Headers;

            var token = headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            var result = ClaimsExtention.Claims(token);
            return result;
        }

        protected long PersonID
        {
            get
            {
                var result = HeaderAuthorization(Request);

                var personID = result.Claims.FirstOrDefault(c => c.Type == "personID").Value;


                return !string.IsNullOrEmpty(personID) ? long.Parse(personID) : 0;
            }


        }

        protected bool IsAdmin(HttpRequest request)
        {
            try
            {
                var result = HeaderAuthorization(request);

                var user = result.Claims.Where(c => c.Type == "user")
                                        .Select(c => c.Value)
                                        .SingleOrDefault();

                return !string.IsNullOrEmpty(user) && user == "Admin" ? true : false;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}