using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using QuickPay.Common.Ententions;
using QuickPay.Crosscut.IoC.Containers;
using QuickPay.Crosscut.Security.JwtProvider;
using QuickPay.Data.Context;
using QuickPay.Presentation.WebApi.AppStart;

namespace QuickPay.Presentation.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            #region Swagger           
            services.InitializeSwagger();
            #endregion

            #region ConnectionDB
            var connection = Configuration.GetConnectionString("DefaultDatabase").Decode();
            services.AddDbContext<QuickPayEntities>(options =>
            {
                options.UseMySql(connection);
            });
            #endregion

            #region IoC
            services.InitializeServiceContainer();
            services.InitializerRepositoryContainer();
            services.InitializeSuportContainer();
            #endregion
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());


            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("./v1/swagger.json", "QuickPay V1");
                c.DocumentTitle = "Developer by Carlos Soares";
            });

            app.UseMiddleware<QuickPayJwtProvider>();

            app.UseEndpoints(x => x.MapControllers());
        }
    }
}
