﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using QuickPay.Service.Adapter.Profiles.BankModule;
using QuickPay.Service.Adapter.Profiles.PersonModule;
using System.Linq;

namespace QuickPay.Service.Adapter
{
    public class AutoMapperObjectAdapter : IAutoMapperObjectAdapter
    {
        #region Fields       
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configuration;
        #endregion

        #region Constructor
        public AutoMapperObjectAdapter()
        {
            _configuration = Config();
            _mapper = _configuration.CreateMapper();
        }
        #endregion

        #region Private Methods
        private MapperConfiguration Config()
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                #region BankModule
                cfg.AddProfile<BankProfile>();
                #endregion

                #region PersonModule
                cfg.AddProfile<PersonProfile>();
                #endregion
            });

            return mapperConfiguration;
        }
        #endregion

        #region Public Methods        
        public TTo Convert<TFrom, TTo>(TFrom source) where TFrom : class
        {
            return _mapper.Map<TFrom, TTo>(source);
        }

        public IQueryable<T> ProjectTo<T>(IQueryable source)
        {
            return source.ProjectTo<T>(_configuration);
        }
        #endregion
    }
}