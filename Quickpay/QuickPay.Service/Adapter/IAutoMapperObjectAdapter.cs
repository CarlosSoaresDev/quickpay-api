﻿using System.Linq;

namespace QuickPay.Service.Adapter
{
    public interface IAutoMapperObjectAdapter
    {
        TTo Convert<TFrom, TTo>(TFrom source) where TFrom : class;
        IQueryable<T> ProjectTo<T>(IQueryable source);
    }
}