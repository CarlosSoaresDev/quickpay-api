﻿using AutoMapper;
using QuickPay.Domain.Entities;
using QuickPay.Service.DTO.BankModule;

namespace QuickPay.Service.Adapter.Profiles.BankModule
{
    public class BankProfile : Profile
    {
        public BankProfile()
        {
            CreateMap<Bank, BankDTO>();
        }
    }
}
