﻿using AutoMapper;
using QuickPay.Domain.Entities;
using QuickPay.Service.DTO.PersonModule;

namespace QuickPay.Service.Adapter.Profiles.PersonModule
{
    public class PersonProfile : Profile
    {
        public PersonProfile()
        {
            CreateMap<Person, PersonDTO>();
        }
    }
}
