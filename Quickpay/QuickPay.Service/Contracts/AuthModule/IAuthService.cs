﻿using QuickPay.Service.DTO.AuthModule;
using QuickPay.Service.DTO.PersonModule;

namespace QuickPay.Service.Contracts.AuthModule
{
    public interface IAuthService
    {
         TokenDTO SignIn(PersonUserDTO model);

         TokenDTO SignUp(PersonUserDTO model);
    }
}
