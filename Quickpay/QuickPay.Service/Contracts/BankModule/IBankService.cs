﻿using QuickPay.Service.DTO.BankModule;
using System.Collections.Generic;

namespace QuickPay.Service.Contracts.BankModule
{
    public interface IBankService
    {
        public IEnumerable<BankDTO> GetAll();

        public IEnumerable<BankDTO> GetAllByFilter(BankDTO bank);

        public BankDTO GetByID(long bankID);

        public long Update(BankDTO model);

        public long Create(BankDTO model);

        public long Delete(long bankID);
    }
}
