﻿using QuickPay.Service.DTO.PersonModule;
using System.Collections.Generic;

namespace QuickPay.Service.Contracts.PersonModule
{
    public interface IPersonService
    {
        public IEnumerable<PersonDTO> GetAll();

        public IEnumerable<PersonDTO> GetAllByFilter(PersonDTO bank);

        public PersonDTO GetByID(long bankID);

        public long Update(PersonDTO model);

        public long Create(PersonDTO model);

        public long Delete(long bankID);
    }
}