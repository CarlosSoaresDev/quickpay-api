﻿using QuickPay.Common.Enuns;

namespace QuickPay.Service.DTO.AuthModule
{
   public class TokenDTO
    {
        public string Token { get; set; }
        public TokenStatusEnum Status { get; set; }
    }
}
