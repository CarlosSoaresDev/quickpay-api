﻿using System;

namespace QuickPay.Service.DTO.BankModule
{
    public class BankDTO
    {
        public long BankID { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public string About { get; set; }
        public string IconUrl { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDateTime { get; set; }
    }
}
