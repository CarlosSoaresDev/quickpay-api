﻿using QuickPay.Common.Enuns;
using System;

namespace QuickPay.Service.DTO.PersonModule
{
   public class PersonDTO
    {
        public long PersonID { get; set; }
        public PersonTypeEnum Type { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhotoUrl { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDateTime { get; set; } 
    }
}
