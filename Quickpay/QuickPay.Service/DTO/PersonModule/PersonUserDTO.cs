﻿namespace QuickPay.Service.DTO.PersonModule
{
    public class PersonUserDTO
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
