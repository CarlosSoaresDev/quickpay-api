﻿using QuickPay.Common.Ententions;
using QuickPay.Crosscut.Security.JwtProvider;
using QuickPay.Data.Repository.Contracts.PersonModule;
using QuickPay.Domain.Entities;
using QuickPay.Service.Adapter;
using QuickPay.Service.Contracts.AuthModule;
using QuickPay.Service.DTO.PersonModule;
using System;
using System.Linq;

namespace QuickPay.Service.DTO.AuthModule
{
    public class AuthService : IAuthService
    {
        #region Fields
        private readonly IPersonRepository _personRepository;
        private readonly IAutoMapperObjectAdapter _objectAdapter;
        #endregion

        #region Constructor
        public AuthService(
            IPersonRepository personRepository,
            IAutoMapperObjectAdapter objectAdapter
            )
        {
            _personRepository = personRepository;
            _objectAdapter = objectAdapter;
        }
        #endregion

        #region Methods

        public TokenDTO SignIn(PersonUserDTO model)
        {
            if (!String.IsNullOrEmpty(model.Email) || !String.IsNullOrEmpty(model.Password))
            {
                var user = _personRepository
                    .Get(g => g.Email == model.Email && g.Password == model.Password.ToHashMd5())
                    .FirstOrDefault();

                if (user.IsNull())
                    throw new Exception("Person user Dont existe");

                if (user.IsActive)
                {
                    
                    string tokenString = QuickPayJwtProvider.GenerateJwtToken(user);

                    return new TokenDTO
                    {
                        Token = tokenString,
                        Status = Common.Enuns.TokenStatusEnum.Accept
                    };
                }

                return new TokenDTO
                {
                    Token = null,
                    Status = Common.Enuns.TokenStatusEnum.InactiveUser
                };
            }
            return new TokenDTO
            {
                Token = null,
                Status = Common.Enuns.TokenStatusEnum.NotAccepted
            };
        }


        public TokenDTO SignUp(PersonUserDTO model)
        {
            var person = new Person()
            {
                Name = model.Name,
                Type = Common.Enuns.PersonTypeEnum.User,
                Email = model.Email,
                PhotoUrl = "https://i1.wp.com/vectorified.com/images/no-profile-picture-icon-21.jpg",
                Password = model.Password.ToHashMd5()
            };

            _personRepository.Add(person);

            string tokenString = QuickPayJwtProvider.GenerateJwtToken(person);

            return new TokenDTO
            {
                Token = tokenString,
                Status = Common.Enuns.TokenStatusEnum.Accept
            };
        }
        #endregion
    }
}