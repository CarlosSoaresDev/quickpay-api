﻿using LinqKit;
using QuickPay.Data.Repository.Contracts.BankModule;
using QuickPay.Domain.Entities;
using QuickPay.Service.Adapter;
using QuickPay.Service.Contracts.BankModule;
using QuickPay.Service.DTO.BankModule;
using System.Collections.Generic;
using System.Linq;

namespace QuickPay.Service.Services.BankModule
{
    public class BankService : IBankService
    {
        #region Fields
        private readonly IBankRepository _bankRepository;
        private readonly IAutoMapperObjectAdapter _objectAdapter;
        #endregion

        #region Constructor
        public BankService(
            IBankRepository bankRepository,
            IAutoMapperObjectAdapter objectAdapter
            )
        {
            _bankRepository = bankRepository;
            _objectAdapter = objectAdapter;
        }
        #endregion

        #region Methods
        public IEnumerable<BankDTO> GetAll()
        {
            var queryBase = _bankRepository
               .Get(g => g.IsActive);

            var banks = _objectAdapter.ProjectTo<BankDTO>(queryBase);

            return banks;
        }

        public IEnumerable<BankDTO> GetAllByFilter(BankDTO bank)
        {
            var predicate = PredicateBuilder.True<BankDTO>();

            predicate = predicate.And(p => p.IsActive == true);

            if (!string.IsNullOrEmpty(bank.Name))
                predicate = predicate.And(p => p.Name.Contains(bank.Name));

            var queryBase = _bankRepository.GetAll();

            var banks = _objectAdapter.ProjectTo<BankDTO>(queryBase);


            return banks;
        }

        public BankDTO GetByID(long bankID)
        {
            var queryBase = _bankRepository.Get(g => g.BankID == bankID);

            var banks = _objectAdapter.ProjectTo<BankDTO>(queryBase);

            return banks.FirstOrDefault();
        }

        public long Update(BankDTO model)
        {
            var bank = _bankRepository
                .Get(g => g.BankID == model.BankID)
                .FirstOrDefault();

            bank.BankID = model.BankID;
            bank.Name = model.Name;
            bank.About = model.About;
            bank.IconUrl = model.IconUrl;
            bank.IsActive = model.IsActive;

            _bankRepository.Update(bank);

            return bank.BankID;
        }

        public long Create(BankDTO model)
        {
            var bank = new Bank()
            {
                BankID = model.BankID,
                Name = model.Name,
                About = model.About,
                IconUrl = model.IconUrl
            };

            _bankRepository.Add(bank);

            return bank.BankID;
        }

        public long Delete(long bankID)
        {
            var bank = _bankRepository
                .Get(g => g.BankID == bankID)
                .FirstOrDefault();

            bank.IsActive = false;
            _bankRepository.Update(bank);

            return bank.BankID;
        }
        #endregion
    }
}