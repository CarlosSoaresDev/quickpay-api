﻿using LinqKit;
using QuickPay.Common.Ententions;
using QuickPay.Data.Repository.Contracts.PersonModule;
using QuickPay.Domain.Entities;
using QuickPay.Service.Adapter;
using QuickPay.Service.Contracts.PersonModule;
using QuickPay.Service.DTO.PersonModule;
using System.Collections.Generic;
using System.Linq;

namespace QuickPay.Service.Services.PersonModule
{
    public class PersonService : IPersonService
    {
        #region Fields
        private readonly IPersonRepository _personRepository;
        private readonly IAutoMapperObjectAdapter _objectAdapter;
        #endregion

        #region Constructor
        public PersonService(
            IPersonRepository personRepository,
            IAutoMapperObjectAdapter objectAdapter
            )
        {
            _personRepository = personRepository;
            _objectAdapter = objectAdapter;
        }
        #endregion

        #region Methods
        public IEnumerable<PersonDTO> GetAll()
        {
            var queryBase = _personRepository
               .Get(g => g.IsActive);

            var banks = _objectAdapter.ProjectTo<PersonDTO>(queryBase);

            return banks;
        }

        public IEnumerable<PersonDTO> GetAllByFilter(PersonDTO bank)
        {
            var predicate = PredicateBuilder.True<PersonDTO>();

            predicate = predicate.And(p => p.IsActive == true);

            if (!string.IsNullOrEmpty(bank.Name))
                predicate = predicate.And(p => p.Name.Contains(bank.Name));

            var queryBase = _personRepository.GetAll();

            var banks = _objectAdapter.ProjectTo<PersonDTO>(queryBase);


            return banks;
        }

        public PersonDTO GetByID(long bankID)
        {
            var queryBase = _personRepository.Get(g => g.PersonID == bankID);

            var banks = _objectAdapter.ProjectTo<PersonDTO>(queryBase);

            return banks.FirstOrDefault();
        }

        public long Update(PersonDTO model)
        {
            var bank = _personRepository
                .Get(g => g.PersonID == model.PersonID)
                .FirstOrDefault();

            bank.PersonID = model.PersonID;
            bank.Name = model.Name;
            bank.Type = model.Type;
            bank.NickName = model.NickName;
            bank.IsActive = model.IsActive;

            _personRepository.Update(bank);

            return bank.PersonID;
        }

        public long Create(PersonDTO model)
        {
            var bank = new Person()
            {
                PersonID = model.PersonID,
                Name = model.Name,
                Type = model.Type,
                Email = model.Email,
                PhotoUrl = model.PhotoUrl,
                Password = model.Password.ToHashMd5(),
               NickName = model.NickName
            };

            _personRepository.Add(bank);

            return bank.PersonID;
        }

        public long Delete(long bankID)
        {
            var bank = _personRepository
                .Get(g => g.PersonID == bankID)
                .FirstOrDefault();

            bank.IsActive = false;

            _personRepository.Update(bank);

            return bank.PersonID;
        }
        #endregion
    }
}